export default class Animal {
  constructor() {
    if (this.constructor === Animal) {
      throw new Error(
        "Class Animal should be extended instead of instantiated"
      );
    }
  }

  speak(content) {
    return content
      .split(" ")
      .flatMap(part => [part, this.sound])
      .join(" ");
  }
}
