import Animal from "../animal";
import Tiger from "../tiger";

describe("Tiger", () => {
  it("is a class", () => {
    expect(Tiger).toBeInstanceOf(Function);
  });

  it("can be instantiated", () => {
    expect(() => new Tiger()).not.toThrow();
  });

  describe("#speak()", () => {
    const speak = Tiger.prototype.speak;

    it("is a function", () => {
      expect(speak).toBeInstanceOf(Function);
    });

    // Since Tiger inherits speak() from Animal, its coverage is enough
    it("is the same of Animal#speak", () => {
      expect(Tiger.prototype.speak).toEqual(Animal.prototype.speak);
    });
  });
});
