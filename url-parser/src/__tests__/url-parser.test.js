import urlParser from "../url-parser";

describe("urlParser", () => {
  it("is a function", () => {
    expect(urlParser).toBeInstanceOf(Function);
  });

  it("requires a valid URL pattern as first parameter", () => {
    const invalidPatterns = ["asdf", "asdf/", "a/b/c", "//asdf", "/asdf/"];

    const validPatterns = [
      "/ab/cd/ef",
      "/:ab/cd/ef",
      "/ab/:dc/ef",
      "/ab/cd/:ef",
      "/:ab/:cd/ef",
      "/ab/:cd/:ef",
      "/:ab/:cd/:ef"
    ];

    invalidPatterns.forEach(pattern =>
      expect(() => urlParser(pattern, "/ab/cd/ef")).toThrow()
    );

    validPatterns.forEach(pattern =>
      expect(() => urlParser(pattern, "/ab/cd/ef")).not.toThrow()
    );
  });

  it("does currying if given a single parameter", () => {
    const validPatterns = ["/abc", "/abc/:def", "/abc/:def/ghi"];

    validPatterns.forEach(pattern =>
      expect(urlParser(pattern)).toBeInstanceOf(Function)
    );
  });

  it("requires a valid URL as second parameter", () => {
    const patterns = [
      {
        pattern: "/abc",
        invalidUrls: ["abc", "abc/", "a/b/c", "//abc", "/abc/"],
        validUrls: ["/abc", "/abc?def", "/abc?def=ghi", "/abc?def=ghi&jkl=mno"]
      },
      {
        pattern: "/abc/:def",
        invalidUrls: ["abc", "abc/", "a/b/c", "//abc", "/abc/"],
        validUrls: ["/abc/def", "/abc/one", "/abc/two?three=4"]
      },
      {
        pattern: "/abc/:def/ghi",
        invalidUrls: ["abc", "abc/", "//abc", "/abc/def", "/abc/def/gi/jk"],
        validUrls: ["/abc/def/ghi", "/abc/one/ghi", "/abc/two/ghi?three=4"]
      }
    ];

    patterns.forEach(({ pattern, invalidUrls, validUrls }) => {
      invalidUrls.forEach(url =>
        expect(() => urlParser(pattern, url)).toThrow()
      );

      validUrls.forEach(url =>
        expect(() => urlParser(pattern, url)).not.toThrow()
      );
    });
  });

  it("extracts variables from path", () => {
    const cases = [
      {
        pattern: "/hey/:you",
        pairs: [
          { url: "/hey/john", variables: { you: "john" } },
          { url: "/hey/joe", variables: { you: "joe" } },
          { url: "/hey/josh", variables: { you: "josh" } }
        ]
      },
      {
        pattern: "/api/:version/cool-items",
        pairs: [
          { url: "/api/1/cool-items", variables: { version: "1" } },
          { url: "/api/2/cool-items", variables: { version: "2" } },
          { url: "/api/3/cool-items", variables: { version: "3" } }
        ]
      },
      {
        pattern: "/:firstName/:lastName",
        pairs: [
          {
            url: "/John/Doe",
            variables: { firstName: "John", lastName: "Doe" }
          },
          {
            url: "/Josh/Doe",
            variables: { firstName: "Josh", lastName: "Doe" }
          },
          {
            url: "/Jane/Doe",
            variables: { firstName: "Jane", lastName: "Doe" }
          }
        ]
      }
    ];

    cases.forEach(({ pattern, pairs }) =>
      pairs.forEach(({ url, variables }) =>
        expect(urlParser(pattern, url)).toEqual(variables)
      )
    );
  });

  it("extracts variables from query", () => {
    const pairs = [
      { url: "/hey?you=john", variables: { you: "john" } },
      { url: "/hey?you=joe", variables: { you: "joe" } },
      { url: "/hey?you=josh", variables: { you: "josh" } },
      { url: "/hey?you=john&me=jane", variables: { you: "john", me: "jane" } }
    ];

    pairs.forEach(({ url, variables }) =>
      expect(urlParser("/hey", url)).toEqual(variables)
    );
  });

  it("works with both path and query together", () => {
    const cases = [
      {
        pattern: "/hey/:you",
        pairs: [
          { url: "/hey/john?me=jane", variables: { you: "john", me: "jane" } },
          { url: "/hey/joe", variables: { you: "joe" } },
          { url: "/hey/josh", variables: { you: "josh" } }
        ]
      },
      {
        pattern: "/api/:version/:repository",
        pairs: [
          {
            url: "/api/1/posts",
            variables: { version: "1", repository: "posts" }
          },
          {
            url: "/api/123/456",
            variables: { version: "123", repository: "456" }
          },
          {
            url: "/api/foo/bar",
            variables: { version: "foo", repository: "bar" }
          }
        ]
      }
    ];

    cases.forEach(({ pattern, pairs }) =>
      pairs.forEach(({ url, variables }) =>
        expect(urlParser(pattern, url)).toEqual(variables)
      )
    );
  });

  it("returns an object", () => {
    expect(urlParser("/asdf", "/asdf")).toBeInstanceOf(Object);
  });
});
